var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __commonJS = (cb, mod) => function __require() {
  return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
  mod
));

// node_modules/big-integer/BigInteger.js
var require_BigInteger = __commonJS({
  "node_modules/big-integer/BigInteger.js"(exports, module) {
    var bigInt3 = function(undefined) {
      "use strict";
      var BASE = 1e7, LOG_BASE = 7, MAX_INT = 9007199254740992, MAX_INT_ARR = smallToArray(MAX_INT), DEFAULT_ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyz";
      var supportsNativeBigInt = typeof BigInt === "function";
      function Integer(v, radix, alphabet, caseSensitive) {
        if (typeof v === "undefined")
          return Integer[0];
        if (typeof radix !== "undefined")
          return +radix === 10 && !alphabet ? parseValue(v) : parseBase(v, radix, alphabet, caseSensitive);
        return parseValue(v);
      }
      function BigInteger(value, sign) {
        this.value = value;
        this.sign = sign;
        this.isSmall = false;
      }
      BigInteger.prototype = Object.create(Integer.prototype);
      function SmallInteger(value) {
        this.value = value;
        this.sign = value < 0;
        this.isSmall = true;
      }
      SmallInteger.prototype = Object.create(Integer.prototype);
      function NativeBigInt(value) {
        this.value = value;
      }
      NativeBigInt.prototype = Object.create(Integer.prototype);
      function isPrecise(n) {
        return -MAX_INT < n && n < MAX_INT;
      }
      function smallToArray(n) {
        if (n < 1e7)
          return [n];
        if (n < 1e14)
          return [n % 1e7, Math.floor(n / 1e7)];
        return [n % 1e7, Math.floor(n / 1e7) % 1e7, Math.floor(n / 1e14)];
      }
      function arrayToSmall(arr) {
        trim(arr);
        var length = arr.length;
        if (length < 4 && compareAbs(arr, MAX_INT_ARR) < 0) {
          switch (length) {
            case 0:
              return 0;
            case 1:
              return arr[0];
            case 2:
              return arr[0] + arr[1] * BASE;
            default:
              return arr[0] + (arr[1] + arr[2] * BASE) * BASE;
          }
        }
        return arr;
      }
      function trim(v) {
        var i2 = v.length;
        while (v[--i2] === 0)
          ;
        v.length = i2 + 1;
      }
      function createArray(length) {
        var x = new Array(length);
        var i2 = -1;
        while (++i2 < length) {
          x[i2] = 0;
        }
        return x;
      }
      function truncate(n) {
        if (n > 0)
          return Math.floor(n);
        return Math.ceil(n);
      }
      function add(a, b) {
        var l_a = a.length, l_b = b.length, r = new Array(l_a), carry = 0, base = BASE, sum, i2;
        for (i2 = 0; i2 < l_b; i2++) {
          sum = a[i2] + b[i2] + carry;
          carry = sum >= base ? 1 : 0;
          r[i2] = sum - carry * base;
        }
        while (i2 < l_a) {
          sum = a[i2] + carry;
          carry = sum === base ? 1 : 0;
          r[i2++] = sum - carry * base;
        }
        if (carry > 0)
          r.push(carry);
        return r;
      }
      function addAny(a, b) {
        if (a.length >= b.length)
          return add(a, b);
        return add(b, a);
      }
      function addSmall(a, carry) {
        var l = a.length, r = new Array(l), base = BASE, sum, i2;
        for (i2 = 0; i2 < l; i2++) {
          sum = a[i2] - base + carry;
          carry = Math.floor(sum / base);
          r[i2] = sum - carry * base;
          carry += 1;
        }
        while (carry > 0) {
          r[i2++] = carry % base;
          carry = Math.floor(carry / base);
        }
        return r;
      }
      BigInteger.prototype.add = function(v) {
        var n = parseValue(v);
        if (this.sign !== n.sign) {
          return this.subtract(n.negate());
        }
        var a = this.value, b = n.value;
        if (n.isSmall) {
          return new BigInteger(addSmall(a, Math.abs(b)), this.sign);
        }
        return new BigInteger(addAny(a, b), this.sign);
      };
      BigInteger.prototype.plus = BigInteger.prototype.add;
      SmallInteger.prototype.add = function(v) {
        var n = parseValue(v);
        var a = this.value;
        if (a < 0 !== n.sign) {
          return this.subtract(n.negate());
        }
        var b = n.value;
        if (n.isSmall) {
          if (isPrecise(a + b))
            return new SmallInteger(a + b);
          b = smallToArray(Math.abs(b));
        }
        return new BigInteger(addSmall(b, Math.abs(a)), a < 0);
      };
      SmallInteger.prototype.plus = SmallInteger.prototype.add;
      NativeBigInt.prototype.add = function(v) {
        return new NativeBigInt(this.value + parseValue(v).value);
      };
      NativeBigInt.prototype.plus = NativeBigInt.prototype.add;
      function subtract(a, b) {
        var a_l = a.length, b_l = b.length, r = new Array(a_l), borrow = 0, base = BASE, i2, difference;
        for (i2 = 0; i2 < b_l; i2++) {
          difference = a[i2] - borrow - b[i2];
          if (difference < 0) {
            difference += base;
            borrow = 1;
          } else
            borrow = 0;
          r[i2] = difference;
        }
        for (i2 = b_l; i2 < a_l; i2++) {
          difference = a[i2] - borrow;
          if (difference < 0)
            difference += base;
          else {
            r[i2++] = difference;
            break;
          }
          r[i2] = difference;
        }
        for (; i2 < a_l; i2++) {
          r[i2] = a[i2];
        }
        trim(r);
        return r;
      }
      function subtractAny(a, b, sign) {
        var value;
        if (compareAbs(a, b) >= 0) {
          value = subtract(a, b);
        } else {
          value = subtract(b, a);
          sign = !sign;
        }
        value = arrayToSmall(value);
        if (typeof value === "number") {
          if (sign)
            value = -value;
          return new SmallInteger(value);
        }
        return new BigInteger(value, sign);
      }
      function subtractSmall(a, b, sign) {
        var l = a.length, r = new Array(l), carry = -b, base = BASE, i2, difference;
        for (i2 = 0; i2 < l; i2++) {
          difference = a[i2] + carry;
          carry = Math.floor(difference / base);
          difference %= base;
          r[i2] = difference < 0 ? difference + base : difference;
        }
        r = arrayToSmall(r);
        if (typeof r === "number") {
          if (sign)
            r = -r;
          return new SmallInteger(r);
        }
        return new BigInteger(r, sign);
      }
      BigInteger.prototype.subtract = function(v) {
        var n = parseValue(v);
        if (this.sign !== n.sign) {
          return this.add(n.negate());
        }
        var a = this.value, b = n.value;
        if (n.isSmall)
          return subtractSmall(a, Math.abs(b), this.sign);
        return subtractAny(a, b, this.sign);
      };
      BigInteger.prototype.minus = BigInteger.prototype.subtract;
      SmallInteger.prototype.subtract = function(v) {
        var n = parseValue(v);
        var a = this.value;
        if (a < 0 !== n.sign) {
          return this.add(n.negate());
        }
        var b = n.value;
        if (n.isSmall) {
          return new SmallInteger(a - b);
        }
        return subtractSmall(b, Math.abs(a), a >= 0);
      };
      SmallInteger.prototype.minus = SmallInteger.prototype.subtract;
      NativeBigInt.prototype.subtract = function(v) {
        return new NativeBigInt(this.value - parseValue(v).value);
      };
      NativeBigInt.prototype.minus = NativeBigInt.prototype.subtract;
      BigInteger.prototype.negate = function() {
        return new BigInteger(this.value, !this.sign);
      };
      SmallInteger.prototype.negate = function() {
        var sign = this.sign;
        var small = new SmallInteger(-this.value);
        small.sign = !sign;
        return small;
      };
      NativeBigInt.prototype.negate = function() {
        return new NativeBigInt(-this.value);
      };
      BigInteger.prototype.abs = function() {
        return new BigInteger(this.value, false);
      };
      SmallInteger.prototype.abs = function() {
        return new SmallInteger(Math.abs(this.value));
      };
      NativeBigInt.prototype.abs = function() {
        return new NativeBigInt(this.value >= 0 ? this.value : -this.value);
      };
      function multiplyLong(a, b) {
        var a_l = a.length, b_l = b.length, l = a_l + b_l, r = createArray(l), base = BASE, product, carry, i2, a_i, b_j;
        for (i2 = 0; i2 < a_l; ++i2) {
          a_i = a[i2];
          for (var j = 0; j < b_l; ++j) {
            b_j = b[j];
            product = a_i * b_j + r[i2 + j];
            carry = Math.floor(product / base);
            r[i2 + j] = product - carry * base;
            r[i2 + j + 1] += carry;
          }
        }
        trim(r);
        return r;
      }
      function multiplySmall(a, b) {
        var l = a.length, r = new Array(l), base = BASE, carry = 0, product, i2;
        for (i2 = 0; i2 < l; i2++) {
          product = a[i2] * b + carry;
          carry = Math.floor(product / base);
          r[i2] = product - carry * base;
        }
        while (carry > 0) {
          r[i2++] = carry % base;
          carry = Math.floor(carry / base);
        }
        return r;
      }
      function shiftLeft(x, n) {
        var r = [];
        while (n-- > 0)
          r.push(0);
        return r.concat(x);
      }
      function multiplyKaratsuba(x, y) {
        var n = Math.max(x.length, y.length);
        if (n <= 30)
          return multiplyLong(x, y);
        n = Math.ceil(n / 2);
        var b = x.slice(n), a = x.slice(0, n), d = y.slice(n), c = y.slice(0, n);
        var ac = multiplyKaratsuba(a, c), bd = multiplyKaratsuba(b, d), abcd = multiplyKaratsuba(addAny(a, b), addAny(c, d));
        var product = addAny(addAny(ac, shiftLeft(subtract(subtract(abcd, ac), bd), n)), shiftLeft(bd, 2 * n));
        trim(product);
        return product;
      }
      function useKaratsuba(l1, l2) {
        return -0.012 * l1 - 0.012 * l2 + 15e-6 * l1 * l2 > 0;
      }
      BigInteger.prototype.multiply = function(v) {
        var n = parseValue(v), a = this.value, b = n.value, sign = this.sign !== n.sign, abs;
        if (n.isSmall) {
          if (b === 0)
            return Integer[0];
          if (b === 1)
            return this;
          if (b === -1)
            return this.negate();
          abs = Math.abs(b);
          if (abs < BASE) {
            return new BigInteger(multiplySmall(a, abs), sign);
          }
          b = smallToArray(abs);
        }
        if (useKaratsuba(a.length, b.length))
          return new BigInteger(multiplyKaratsuba(a, b), sign);
        return new BigInteger(multiplyLong(a, b), sign);
      };
      BigInteger.prototype.times = BigInteger.prototype.multiply;
      function multiplySmallAndArray(a, b, sign) {
        if (a < BASE) {
          return new BigInteger(multiplySmall(b, a), sign);
        }
        return new BigInteger(multiplyLong(b, smallToArray(a)), sign);
      }
      SmallInteger.prototype._multiplyBySmall = function(a) {
        if (isPrecise(a.value * this.value)) {
          return new SmallInteger(a.value * this.value);
        }
        return multiplySmallAndArray(Math.abs(a.value), smallToArray(Math.abs(this.value)), this.sign !== a.sign);
      };
      BigInteger.prototype._multiplyBySmall = function(a) {
        if (a.value === 0)
          return Integer[0];
        if (a.value === 1)
          return this;
        if (a.value === -1)
          return this.negate();
        return multiplySmallAndArray(Math.abs(a.value), this.value, this.sign !== a.sign);
      };
      SmallInteger.prototype.multiply = function(v) {
        return parseValue(v)._multiplyBySmall(this);
      };
      SmallInteger.prototype.times = SmallInteger.prototype.multiply;
      NativeBigInt.prototype.multiply = function(v) {
        return new NativeBigInt(this.value * parseValue(v).value);
      };
      NativeBigInt.prototype.times = NativeBigInt.prototype.multiply;
      function square(a) {
        var l = a.length, r = createArray(l + l), base = BASE, product, carry, i2, a_i, a_j;
        for (i2 = 0; i2 < l; i2++) {
          a_i = a[i2];
          carry = 0 - a_i * a_i;
          for (var j = i2; j < l; j++) {
            a_j = a[j];
            product = 2 * (a_i * a_j) + r[i2 + j] + carry;
            carry = Math.floor(product / base);
            r[i2 + j] = product - carry * base;
          }
          r[i2 + l] = carry;
        }
        trim(r);
        return r;
      }
      BigInteger.prototype.square = function() {
        return new BigInteger(square(this.value), false);
      };
      SmallInteger.prototype.square = function() {
        var value = this.value * this.value;
        if (isPrecise(value))
          return new SmallInteger(value);
        return new BigInteger(square(smallToArray(Math.abs(this.value))), false);
      };
      NativeBigInt.prototype.square = function(v) {
        return new NativeBigInt(this.value * this.value);
      };
      function divMod1(a, b) {
        var a_l = a.length, b_l = b.length, base = BASE, result = createArray(b.length), divisorMostSignificantDigit = b[b_l - 1], lambda = Math.ceil(base / (2 * divisorMostSignificantDigit)), remainder = multiplySmall(a, lambda), divisor = multiplySmall(b, lambda), quotientDigit, shift, carry, borrow, i2, l, q;
        if (remainder.length <= a_l)
          remainder.push(0);
        divisor.push(0);
        divisorMostSignificantDigit = divisor[b_l - 1];
        for (shift = a_l - b_l; shift >= 0; shift--) {
          quotientDigit = base - 1;
          if (remainder[shift + b_l] !== divisorMostSignificantDigit) {
            quotientDigit = Math.floor((remainder[shift + b_l] * base + remainder[shift + b_l - 1]) / divisorMostSignificantDigit);
          }
          carry = 0;
          borrow = 0;
          l = divisor.length;
          for (i2 = 0; i2 < l; i2++) {
            carry += quotientDigit * divisor[i2];
            q = Math.floor(carry / base);
            borrow += remainder[shift + i2] - (carry - q * base);
            carry = q;
            if (borrow < 0) {
              remainder[shift + i2] = borrow + base;
              borrow = -1;
            } else {
              remainder[shift + i2] = borrow;
              borrow = 0;
            }
          }
          while (borrow !== 0) {
            quotientDigit -= 1;
            carry = 0;
            for (i2 = 0; i2 < l; i2++) {
              carry += remainder[shift + i2] - base + divisor[i2];
              if (carry < 0) {
                remainder[shift + i2] = carry + base;
                carry = 0;
              } else {
                remainder[shift + i2] = carry;
                carry = 1;
              }
            }
            borrow += carry;
          }
          result[shift] = quotientDigit;
        }
        remainder = divModSmall(remainder, lambda)[0];
        return [arrayToSmall(result), arrayToSmall(remainder)];
      }
      function divMod2(a, b) {
        var a_l = a.length, b_l = b.length, result = [], part = [], base = BASE, guess, xlen, highx, highy, check;
        while (a_l) {
          part.unshift(a[--a_l]);
          trim(part);
          if (compareAbs(part, b) < 0) {
            result.push(0);
            continue;
          }
          xlen = part.length;
          highx = part[xlen - 1] * base + part[xlen - 2];
          highy = b[b_l - 1] * base + b[b_l - 2];
          if (xlen > b_l) {
            highx = (highx + 1) * base;
          }
          guess = Math.ceil(highx / highy);
          do {
            check = multiplySmall(b, guess);
            if (compareAbs(check, part) <= 0)
              break;
            guess--;
          } while (guess);
          result.push(guess);
          part = subtract(part, check);
        }
        result.reverse();
        return [arrayToSmall(result), arrayToSmall(part)];
      }
      function divModSmall(value, lambda) {
        var length = value.length, quotient = createArray(length), base = BASE, i2, q, remainder, divisor;
        remainder = 0;
        for (i2 = length - 1; i2 >= 0; --i2) {
          divisor = remainder * base + value[i2];
          q = truncate(divisor / lambda);
          remainder = divisor - q * lambda;
          quotient[i2] = q | 0;
        }
        return [quotient, remainder | 0];
      }
      function divModAny(self, v) {
        var value, n = parseValue(v);
        if (supportsNativeBigInt) {
          return [new NativeBigInt(self.value / n.value), new NativeBigInt(self.value % n.value)];
        }
        var a = self.value, b = n.value;
        var quotient;
        if (b === 0)
          throw new Error("Cannot divide by zero");
        if (self.isSmall) {
          if (n.isSmall) {
            return [new SmallInteger(truncate(a / b)), new SmallInteger(a % b)];
          }
          return [Integer[0], self];
        }
        if (n.isSmall) {
          if (b === 1)
            return [self, Integer[0]];
          if (b == -1)
            return [self.negate(), Integer[0]];
          var abs = Math.abs(b);
          if (abs < BASE) {
            value = divModSmall(a, abs);
            quotient = arrayToSmall(value[0]);
            var remainder = value[1];
            if (self.sign)
              remainder = -remainder;
            if (typeof quotient === "number") {
              if (self.sign !== n.sign)
                quotient = -quotient;
              return [new SmallInteger(quotient), new SmallInteger(remainder)];
            }
            return [new BigInteger(quotient, self.sign !== n.sign), new SmallInteger(remainder)];
          }
          b = smallToArray(abs);
        }
        var comparison = compareAbs(a, b);
        if (comparison === -1)
          return [Integer[0], self];
        if (comparison === 0)
          return [Integer[self.sign === n.sign ? 1 : -1], Integer[0]];
        if (a.length + b.length <= 200)
          value = divMod1(a, b);
        else
          value = divMod2(a, b);
        quotient = value[0];
        var qSign = self.sign !== n.sign, mod = value[1], mSign = self.sign;
        if (typeof quotient === "number") {
          if (qSign)
            quotient = -quotient;
          quotient = new SmallInteger(quotient);
        } else
          quotient = new BigInteger(quotient, qSign);
        if (typeof mod === "number") {
          if (mSign)
            mod = -mod;
          mod = new SmallInteger(mod);
        } else
          mod = new BigInteger(mod, mSign);
        return [quotient, mod];
      }
      BigInteger.prototype.divmod = function(v) {
        var result = divModAny(this, v);
        return {
          quotient: result[0],
          remainder: result[1]
        };
      };
      NativeBigInt.prototype.divmod = SmallInteger.prototype.divmod = BigInteger.prototype.divmod;
      BigInteger.prototype.divide = function(v) {
        return divModAny(this, v)[0];
      };
      NativeBigInt.prototype.over = NativeBigInt.prototype.divide = function(v) {
        return new NativeBigInt(this.value / parseValue(v).value);
      };
      SmallInteger.prototype.over = SmallInteger.prototype.divide = BigInteger.prototype.over = BigInteger.prototype.divide;
      BigInteger.prototype.mod = function(v) {
        return divModAny(this, v)[1];
      };
      NativeBigInt.prototype.mod = NativeBigInt.prototype.remainder = function(v) {
        return new NativeBigInt(this.value % parseValue(v).value);
      };
      SmallInteger.prototype.remainder = SmallInteger.prototype.mod = BigInteger.prototype.remainder = BigInteger.prototype.mod;
      BigInteger.prototype.pow = function(v) {
        var n = parseValue(v), a = this.value, b = n.value, value, x, y;
        if (b === 0)
          return Integer[1];
        if (a === 0)
          return Integer[0];
        if (a === 1)
          return Integer[1];
        if (a === -1)
          return n.isEven() ? Integer[1] : Integer[-1];
        if (n.sign) {
          return Integer[0];
        }
        if (!n.isSmall)
          throw new Error("The exponent " + n.toString() + " is too large.");
        if (this.isSmall) {
          if (isPrecise(value = Math.pow(a, b)))
            return new SmallInteger(truncate(value));
        }
        x = this;
        y = Integer[1];
        while (true) {
          if (b & true) {
            y = y.times(x);
            --b;
          }
          if (b === 0)
            break;
          b /= 2;
          x = x.square();
        }
        return y;
      };
      SmallInteger.prototype.pow = BigInteger.prototype.pow;
      NativeBigInt.prototype.pow = function(v) {
        var n = parseValue(v);
        var a = this.value, b = n.value;
        var _0 = BigInt(0), _1 = BigInt(1), _2 = BigInt(2);
        if (b === _0)
          return Integer[1];
        if (a === _0)
          return Integer[0];
        if (a === _1)
          return Integer[1];
        if (a === BigInt(-1))
          return n.isEven() ? Integer[1] : Integer[-1];
        if (n.isNegative())
          return new NativeBigInt(_0);
        var x = this;
        var y = Integer[1];
        while (true) {
          if ((b & _1) === _1) {
            y = y.times(x);
            --b;
          }
          if (b === _0)
            break;
          b /= _2;
          x = x.square();
        }
        return y;
      };
      BigInteger.prototype.modPow = function(exp, mod) {
        exp = parseValue(exp);
        mod = parseValue(mod);
        if (mod.isZero())
          throw new Error("Cannot take modPow with modulus 0");
        var r = Integer[1], base = this.mod(mod);
        if (exp.isNegative()) {
          exp = exp.multiply(Integer[-1]);
          base = base.modInv(mod);
        }
        while (exp.isPositive()) {
          if (base.isZero())
            return Integer[0];
          if (exp.isOdd())
            r = r.multiply(base).mod(mod);
          exp = exp.divide(2);
          base = base.square().mod(mod);
        }
        return r;
      };
      NativeBigInt.prototype.modPow = SmallInteger.prototype.modPow = BigInteger.prototype.modPow;
      function compareAbs(a, b) {
        if (a.length !== b.length) {
          return a.length > b.length ? 1 : -1;
        }
        for (var i2 = a.length - 1; i2 >= 0; i2--) {
          if (a[i2] !== b[i2])
            return a[i2] > b[i2] ? 1 : -1;
        }
        return 0;
      }
      BigInteger.prototype.compareAbs = function(v) {
        var n = parseValue(v), a = this.value, b = n.value;
        if (n.isSmall)
          return 1;
        return compareAbs(a, b);
      };
      SmallInteger.prototype.compareAbs = function(v) {
        var n = parseValue(v), a = Math.abs(this.value), b = n.value;
        if (n.isSmall) {
          b = Math.abs(b);
          return a === b ? 0 : a > b ? 1 : -1;
        }
        return -1;
      };
      NativeBigInt.prototype.compareAbs = function(v) {
        var a = this.value;
        var b = parseValue(v).value;
        a = a >= 0 ? a : -a;
        b = b >= 0 ? b : -b;
        return a === b ? 0 : a > b ? 1 : -1;
      };
      BigInteger.prototype.compare = function(v) {
        if (v === Infinity) {
          return -1;
        }
        if (v === -Infinity) {
          return 1;
        }
        var n = parseValue(v), a = this.value, b = n.value;
        if (this.sign !== n.sign) {
          return n.sign ? 1 : -1;
        }
        if (n.isSmall) {
          return this.sign ? -1 : 1;
        }
        return compareAbs(a, b) * (this.sign ? -1 : 1);
      };
      BigInteger.prototype.compareTo = BigInteger.prototype.compare;
      SmallInteger.prototype.compare = function(v) {
        if (v === Infinity) {
          return -1;
        }
        if (v === -Infinity) {
          return 1;
        }
        var n = parseValue(v), a = this.value, b = n.value;
        if (n.isSmall) {
          return a == b ? 0 : a > b ? 1 : -1;
        }
        if (a < 0 !== n.sign) {
          return a < 0 ? -1 : 1;
        }
        return a < 0 ? 1 : -1;
      };
      SmallInteger.prototype.compareTo = SmallInteger.prototype.compare;
      NativeBigInt.prototype.compare = function(v) {
        if (v === Infinity) {
          return -1;
        }
        if (v === -Infinity) {
          return 1;
        }
        var a = this.value;
        var b = parseValue(v).value;
        return a === b ? 0 : a > b ? 1 : -1;
      };
      NativeBigInt.prototype.compareTo = NativeBigInt.prototype.compare;
      BigInteger.prototype.equals = function(v) {
        return this.compare(v) === 0;
      };
      NativeBigInt.prototype.eq = NativeBigInt.prototype.equals = SmallInteger.prototype.eq = SmallInteger.prototype.equals = BigInteger.prototype.eq = BigInteger.prototype.equals;
      BigInteger.prototype.notEquals = function(v) {
        return this.compare(v) !== 0;
      };
      NativeBigInt.prototype.neq = NativeBigInt.prototype.notEquals = SmallInteger.prototype.neq = SmallInteger.prototype.notEquals = BigInteger.prototype.neq = BigInteger.prototype.notEquals;
      BigInteger.prototype.greater = function(v) {
        return this.compare(v) > 0;
      };
      NativeBigInt.prototype.gt = NativeBigInt.prototype.greater = SmallInteger.prototype.gt = SmallInteger.prototype.greater = BigInteger.prototype.gt = BigInteger.prototype.greater;
      BigInteger.prototype.lesser = function(v) {
        return this.compare(v) < 0;
      };
      NativeBigInt.prototype.lt = NativeBigInt.prototype.lesser = SmallInteger.prototype.lt = SmallInteger.prototype.lesser = BigInteger.prototype.lt = BigInteger.prototype.lesser;
      BigInteger.prototype.greaterOrEquals = function(v) {
        return this.compare(v) >= 0;
      };
      NativeBigInt.prototype.geq = NativeBigInt.prototype.greaterOrEquals = SmallInteger.prototype.geq = SmallInteger.prototype.greaterOrEquals = BigInteger.prototype.geq = BigInteger.prototype.greaterOrEquals;
      BigInteger.prototype.lesserOrEquals = function(v) {
        return this.compare(v) <= 0;
      };
      NativeBigInt.prototype.leq = NativeBigInt.prototype.lesserOrEquals = SmallInteger.prototype.leq = SmallInteger.prototype.lesserOrEquals = BigInteger.prototype.leq = BigInteger.prototype.lesserOrEquals;
      BigInteger.prototype.isEven = function() {
        return (this.value[0] & 1) === 0;
      };
      SmallInteger.prototype.isEven = function() {
        return (this.value & 1) === 0;
      };
      NativeBigInt.prototype.isEven = function() {
        return (this.value & BigInt(1)) === BigInt(0);
      };
      BigInteger.prototype.isOdd = function() {
        return (this.value[0] & 1) === 1;
      };
      SmallInteger.prototype.isOdd = function() {
        return (this.value & 1) === 1;
      };
      NativeBigInt.prototype.isOdd = function() {
        return (this.value & BigInt(1)) === BigInt(1);
      };
      BigInteger.prototype.isPositive = function() {
        return !this.sign;
      };
      SmallInteger.prototype.isPositive = function() {
        return this.value > 0;
      };
      NativeBigInt.prototype.isPositive = SmallInteger.prototype.isPositive;
      BigInteger.prototype.isNegative = function() {
        return this.sign;
      };
      SmallInteger.prototype.isNegative = function() {
        return this.value < 0;
      };
      NativeBigInt.prototype.isNegative = SmallInteger.prototype.isNegative;
      BigInteger.prototype.isUnit = function() {
        return false;
      };
      SmallInteger.prototype.isUnit = function() {
        return Math.abs(this.value) === 1;
      };
      NativeBigInt.prototype.isUnit = function() {
        return this.abs().value === BigInt(1);
      };
      BigInteger.prototype.isZero = function() {
        return false;
      };
      SmallInteger.prototype.isZero = function() {
        return this.value === 0;
      };
      NativeBigInt.prototype.isZero = function() {
        return this.value === BigInt(0);
      };
      BigInteger.prototype.isDivisibleBy = function(v) {
        var n = parseValue(v);
        if (n.isZero())
          return false;
        if (n.isUnit())
          return true;
        if (n.compareAbs(2) === 0)
          return this.isEven();
        return this.mod(n).isZero();
      };
      NativeBigInt.prototype.isDivisibleBy = SmallInteger.prototype.isDivisibleBy = BigInteger.prototype.isDivisibleBy;
      function isBasicPrime(v) {
        var n = v.abs();
        if (n.isUnit())
          return false;
        if (n.equals(2) || n.equals(3) || n.equals(5))
          return true;
        if (n.isEven() || n.isDivisibleBy(3) || n.isDivisibleBy(5))
          return false;
        if (n.lesser(49))
          return true;
      }
      function millerRabinTest(n, a) {
        var nPrev = n.prev(), b = nPrev, r = 0, d, t, i2, x;
        while (b.isEven())
          b = b.divide(2), r++;
        next:
          for (i2 = 0; i2 < a.length; i2++) {
            if (n.lesser(a[i2]))
              continue;
            x = bigInt3(a[i2]).modPow(b, n);
            if (x.isUnit() || x.equals(nPrev))
              continue;
            for (d = r - 1; d != 0; d--) {
              x = x.square().mod(n);
              if (x.isUnit())
                return false;
              if (x.equals(nPrev))
                continue next;
            }
            return false;
          }
        return true;
      }
      BigInteger.prototype.isPrime = function(strict) {
        var isPrime = isBasicPrime(this);
        if (isPrime !== undefined)
          return isPrime;
        var n = this.abs();
        var bits = n.bitLength();
        if (bits <= 64)
          return millerRabinTest(n, [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]);
        var logN = Math.log(2) * bits.toJSNumber();
        var t = Math.ceil(strict === true ? 2 * Math.pow(logN, 2) : logN);
        for (var a = [], i2 = 0; i2 < t; i2++) {
          a.push(bigInt3(i2 + 2));
        }
        return millerRabinTest(n, a);
      };
      NativeBigInt.prototype.isPrime = SmallInteger.prototype.isPrime = BigInteger.prototype.isPrime;
      BigInteger.prototype.isProbablePrime = function(iterations, rng) {
        var isPrime = isBasicPrime(this);
        if (isPrime !== undefined)
          return isPrime;
        var n = this.abs();
        var t = iterations === undefined ? 5 : iterations;
        for (var a = [], i2 = 0; i2 < t; i2++) {
          a.push(bigInt3.randBetween(2, n.minus(2), rng));
        }
        return millerRabinTest(n, a);
      };
      NativeBigInt.prototype.isProbablePrime = SmallInteger.prototype.isProbablePrime = BigInteger.prototype.isProbablePrime;
      BigInteger.prototype.modInv = function(n) {
        var t = bigInt3.zero, newT = bigInt3.one, r = parseValue(n), newR = this.abs(), q, lastT, lastR;
        while (!newR.isZero()) {
          q = r.divide(newR);
          lastT = t;
          lastR = r;
          t = newT;
          r = newR;
          newT = lastT.subtract(q.multiply(newT));
          newR = lastR.subtract(q.multiply(newR));
        }
        if (!r.isUnit())
          throw new Error(this.toString() + " and " + n.toString() + " are not co-prime");
        if (t.compare(0) === -1) {
          t = t.add(n);
        }
        if (this.isNegative()) {
          return t.negate();
        }
        return t;
      };
      NativeBigInt.prototype.modInv = SmallInteger.prototype.modInv = BigInteger.prototype.modInv;
      BigInteger.prototype.next = function() {
        var value = this.value;
        if (this.sign) {
          return subtractSmall(value, 1, this.sign);
        }
        return new BigInteger(addSmall(value, 1), this.sign);
      };
      SmallInteger.prototype.next = function() {
        var value = this.value;
        if (value + 1 < MAX_INT)
          return new SmallInteger(value + 1);
        return new BigInteger(MAX_INT_ARR, false);
      };
      NativeBigInt.prototype.next = function() {
        return new NativeBigInt(this.value + BigInt(1));
      };
      BigInteger.prototype.prev = function() {
        var value = this.value;
        if (this.sign) {
          return new BigInteger(addSmall(value, 1), true);
        }
        return subtractSmall(value, 1, this.sign);
      };
      SmallInteger.prototype.prev = function() {
        var value = this.value;
        if (value - 1 > -MAX_INT)
          return new SmallInteger(value - 1);
        return new BigInteger(MAX_INT_ARR, true);
      };
      NativeBigInt.prototype.prev = function() {
        return new NativeBigInt(this.value - BigInt(1));
      };
      var powersOfTwo = [1];
      while (2 * powersOfTwo[powersOfTwo.length - 1] <= BASE)
        powersOfTwo.push(2 * powersOfTwo[powersOfTwo.length - 1]);
      var powers2Length = powersOfTwo.length, highestPower2 = powersOfTwo[powers2Length - 1];
      function shift_isSmall(n) {
        return Math.abs(n) <= BASE;
      }
      BigInteger.prototype.shiftLeft = function(v) {
        var n = parseValue(v).toJSNumber();
        if (!shift_isSmall(n)) {
          throw new Error(String(n) + " is too large for shifting.");
        }
        if (n < 0)
          return this.shiftRight(-n);
        var result = this;
        if (result.isZero())
          return result;
        while (n >= powers2Length) {
          result = result.multiply(highestPower2);
          n -= powers2Length - 1;
        }
        return result.multiply(powersOfTwo[n]);
      };
      NativeBigInt.prototype.shiftLeft = SmallInteger.prototype.shiftLeft = BigInteger.prototype.shiftLeft;
      BigInteger.prototype.shiftRight = function(v) {
        var remQuo;
        var n = parseValue(v).toJSNumber();
        if (!shift_isSmall(n)) {
          throw new Error(String(n) + " is too large for shifting.");
        }
        if (n < 0)
          return this.shiftLeft(-n);
        var result = this;
        while (n >= powers2Length) {
          if (result.isZero() || result.isNegative() && result.isUnit())
            return result;
          remQuo = divModAny(result, highestPower2);
          result = remQuo[1].isNegative() ? remQuo[0].prev() : remQuo[0];
          n -= powers2Length - 1;
        }
        remQuo = divModAny(result, powersOfTwo[n]);
        return remQuo[1].isNegative() ? remQuo[0].prev() : remQuo[0];
      };
      NativeBigInt.prototype.shiftRight = SmallInteger.prototype.shiftRight = BigInteger.prototype.shiftRight;
      function bitwise(x, y, fn) {
        y = parseValue(y);
        var xSign = x.isNegative(), ySign = y.isNegative();
        var xRem = xSign ? x.not() : x, yRem = ySign ? y.not() : y;
        var xDigit = 0, yDigit = 0;
        var xDivMod = null, yDivMod = null;
        var result = [];
        while (!xRem.isZero() || !yRem.isZero()) {
          xDivMod = divModAny(xRem, highestPower2);
          xDigit = xDivMod[1].toJSNumber();
          if (xSign) {
            xDigit = highestPower2 - 1 - xDigit;
          }
          yDivMod = divModAny(yRem, highestPower2);
          yDigit = yDivMod[1].toJSNumber();
          if (ySign) {
            yDigit = highestPower2 - 1 - yDigit;
          }
          xRem = xDivMod[0];
          yRem = yDivMod[0];
          result.push(fn(xDigit, yDigit));
        }
        var sum = fn(xSign ? 1 : 0, ySign ? 1 : 0) !== 0 ? bigInt3(-1) : bigInt3(0);
        for (var i2 = result.length - 1; i2 >= 0; i2 -= 1) {
          sum = sum.multiply(highestPower2).add(bigInt3(result[i2]));
        }
        return sum;
      }
      BigInteger.prototype.not = function() {
        return this.negate().prev();
      };
      NativeBigInt.prototype.not = SmallInteger.prototype.not = BigInteger.prototype.not;
      BigInteger.prototype.and = function(n) {
        return bitwise(this, n, function(a, b) {
          return a & b;
        });
      };
      NativeBigInt.prototype.and = SmallInteger.prototype.and = BigInteger.prototype.and;
      BigInteger.prototype.or = function(n) {
        return bitwise(this, n, function(a, b) {
          return a | b;
        });
      };
      NativeBigInt.prototype.or = SmallInteger.prototype.or = BigInteger.prototype.or;
      BigInteger.prototype.xor = function(n) {
        return bitwise(this, n, function(a, b) {
          return a ^ b;
        });
      };
      NativeBigInt.prototype.xor = SmallInteger.prototype.xor = BigInteger.prototype.xor;
      var LOBMASK_I = 1 << 30, LOBMASK_BI = (BASE & -BASE) * (BASE & -BASE) | LOBMASK_I;
      function roughLOB(n) {
        var v = n.value, x = typeof v === "number" ? v | LOBMASK_I : typeof v === "bigint" ? v | BigInt(LOBMASK_I) : v[0] + v[1] * BASE | LOBMASK_BI;
        return x & -x;
      }
      function integerLogarithm(value, base) {
        if (base.compareTo(value) <= 0) {
          var tmp = integerLogarithm(value, base.square(base));
          var p = tmp.p;
          var e = tmp.e;
          var t = p.multiply(base);
          return t.compareTo(value) <= 0 ? { p: t, e: e * 2 + 1 } : { p, e: e * 2 };
        }
        return { p: bigInt3(1), e: 0 };
      }
      BigInteger.prototype.bitLength = function() {
        var n = this;
        if (n.compareTo(bigInt3(0)) < 0) {
          n = n.negate().subtract(bigInt3(1));
        }
        if (n.compareTo(bigInt3(0)) === 0) {
          return bigInt3(0);
        }
        return bigInt3(integerLogarithm(n, bigInt3(2)).e).add(bigInt3(1));
      };
      NativeBigInt.prototype.bitLength = SmallInteger.prototype.bitLength = BigInteger.prototype.bitLength;
      function max(a, b) {
        a = parseValue(a);
        b = parseValue(b);
        return a.greater(b) ? a : b;
      }
      function min(a, b) {
        a = parseValue(a);
        b = parseValue(b);
        return a.lesser(b) ? a : b;
      }
      function gcd(a, b) {
        a = parseValue(a).abs();
        b = parseValue(b).abs();
        if (a.equals(b))
          return a;
        if (a.isZero())
          return b;
        if (b.isZero())
          return a;
        var c = Integer[1], d, t;
        while (a.isEven() && b.isEven()) {
          d = min(roughLOB(a), roughLOB(b));
          a = a.divide(d);
          b = b.divide(d);
          c = c.multiply(d);
        }
        while (a.isEven()) {
          a = a.divide(roughLOB(a));
        }
        do {
          while (b.isEven()) {
            b = b.divide(roughLOB(b));
          }
          if (a.greater(b)) {
            t = b;
            b = a;
            a = t;
          }
          b = b.subtract(a);
        } while (!b.isZero());
        return c.isUnit() ? a : a.multiply(c);
      }
      function lcm(a, b) {
        a = parseValue(a).abs();
        b = parseValue(b).abs();
        return a.divide(gcd(a, b)).multiply(b);
      }
      function randBetween(a, b, rng) {
        a = parseValue(a);
        b = parseValue(b);
        var usedRNG = rng || Math.random;
        var low = min(a, b), high = max(a, b);
        var range = high.subtract(low).add(1);
        if (range.isSmall)
          return low.add(Math.floor(usedRNG() * range));
        var digits = toBase(range, BASE).value;
        var result = [], restricted = true;
        for (var i2 = 0; i2 < digits.length; i2++) {
          var top = restricted ? digits[i2] + (i2 + 1 < digits.length ? digits[i2 + 1] / BASE : 0) : BASE;
          var digit = truncate(usedRNG() * top);
          result.push(digit);
          if (digit < digits[i2])
            restricted = false;
        }
        return low.add(Integer.fromArray(result, BASE, false));
      }
      var parseBase = function(text, base, alphabet, caseSensitive) {
        alphabet = alphabet || DEFAULT_ALPHABET;
        text = String(text);
        if (!caseSensitive) {
          text = text.toLowerCase();
          alphabet = alphabet.toLowerCase();
        }
        var length = text.length;
        var i2;
        var absBase = Math.abs(base);
        var alphabetValues = {};
        for (i2 = 0; i2 < alphabet.length; i2++) {
          alphabetValues[alphabet[i2]] = i2;
        }
        for (i2 = 0; i2 < length; i2++) {
          var c = text[i2];
          if (c === "-")
            continue;
          if (c in alphabetValues) {
            if (alphabetValues[c] >= absBase) {
              if (c === "1" && absBase === 1)
                continue;
              throw new Error(c + " is not a valid digit in base " + base + ".");
            }
          }
        }
        base = parseValue(base);
        var digits = [];
        var isNegative = text[0] === "-";
        for (i2 = isNegative ? 1 : 0; i2 < text.length; i2++) {
          var c = text[i2];
          if (c in alphabetValues)
            digits.push(parseValue(alphabetValues[c]));
          else if (c === "<") {
            var start = i2;
            do {
              i2++;
            } while (text[i2] !== ">" && i2 < text.length);
            digits.push(parseValue(text.slice(start + 1, i2)));
          } else
            throw new Error(c + " is not a valid character");
        }
        return parseBaseFromArray(digits, base, isNegative);
      };
      function parseBaseFromArray(digits, base, isNegative) {
        var val = Integer[0], pow = Integer[1], i2;
        for (i2 = digits.length - 1; i2 >= 0; i2--) {
          val = val.add(digits[i2].times(pow));
          pow = pow.times(base);
        }
        return isNegative ? val.negate() : val;
      }
      function stringify(digit, alphabet) {
        alphabet = alphabet || DEFAULT_ALPHABET;
        if (digit < alphabet.length) {
          return alphabet[digit];
        }
        return "<" + digit + ">";
      }
      function toBase(n, base) {
        base = bigInt3(base);
        if (base.isZero()) {
          if (n.isZero())
            return { value: [0], isNegative: false };
          throw new Error("Cannot convert nonzero numbers to base 0.");
        }
        if (base.equals(-1)) {
          if (n.isZero())
            return { value: [0], isNegative: false };
          if (n.isNegative())
            return {
              value: [].concat.apply(
                [],
                Array.apply(null, Array(-n.toJSNumber())).map(Array.prototype.valueOf, [1, 0])
              ),
              isNegative: false
            };
          var arr = Array.apply(null, Array(n.toJSNumber() - 1)).map(Array.prototype.valueOf, [0, 1]);
          arr.unshift([1]);
          return {
            value: [].concat.apply([], arr),
            isNegative: false
          };
        }
        var neg = false;
        if (n.isNegative() && base.isPositive()) {
          neg = true;
          n = n.abs();
        }
        if (base.isUnit()) {
          if (n.isZero())
            return { value: [0], isNegative: false };
          return {
            value: Array.apply(null, Array(n.toJSNumber())).map(Number.prototype.valueOf, 1),
            isNegative: neg
          };
        }
        var out = [];
        var left = n, divmod;
        while (left.isNegative() || left.compareAbs(base) >= 0) {
          divmod = left.divmod(base);
          left = divmod.quotient;
          var digit = divmod.remainder;
          if (digit.isNegative()) {
            digit = base.minus(digit).abs();
            left = left.next();
          }
          out.push(digit.toJSNumber());
        }
        out.push(left.toJSNumber());
        return { value: out.reverse(), isNegative: neg };
      }
      function toBaseString(n, base, alphabet) {
        var arr = toBase(n, base);
        return (arr.isNegative ? "-" : "") + arr.value.map(function(x) {
          return stringify(x, alphabet);
        }).join("");
      }
      BigInteger.prototype.toArray = function(radix) {
        return toBase(this, radix);
      };
      SmallInteger.prototype.toArray = function(radix) {
        return toBase(this, radix);
      };
      NativeBigInt.prototype.toArray = function(radix) {
        return toBase(this, radix);
      };
      BigInteger.prototype.toString = function(radix, alphabet) {
        if (radix === undefined)
          radix = 10;
        if (radix !== 10 || alphabet)
          return toBaseString(this, radix, alphabet);
        var v = this.value, l = v.length, str = String(v[--l]), zeros = "0000000", digit;
        while (--l >= 0) {
          digit = String(v[l]);
          str += zeros.slice(digit.length) + digit;
        }
        var sign = this.sign ? "-" : "";
        return sign + str;
      };
      SmallInteger.prototype.toString = function(radix, alphabet) {
        if (radix === undefined)
          radix = 10;
        if (radix != 10 || alphabet)
          return toBaseString(this, radix, alphabet);
        return String(this.value);
      };
      NativeBigInt.prototype.toString = SmallInteger.prototype.toString;
      NativeBigInt.prototype.toJSON = BigInteger.prototype.toJSON = SmallInteger.prototype.toJSON = function() {
        return this.toString();
      };
      BigInteger.prototype.valueOf = function() {
        return parseInt(this.toString(), 10);
      };
      BigInteger.prototype.toJSNumber = BigInteger.prototype.valueOf;
      SmallInteger.prototype.valueOf = function() {
        return this.value;
      };
      SmallInteger.prototype.toJSNumber = SmallInteger.prototype.valueOf;
      NativeBigInt.prototype.valueOf = NativeBigInt.prototype.toJSNumber = function() {
        return parseInt(this.toString(), 10);
      };
      function parseStringValue(v) {
        if (isPrecise(+v)) {
          var x = +v;
          if (x === truncate(x))
            return supportsNativeBigInt ? new NativeBigInt(BigInt(x)) : new SmallInteger(x);
          throw new Error("Invalid integer: " + v);
        }
        var sign = v[0] === "-";
        if (sign)
          v = v.slice(1);
        var split = v.split(/e/i);
        if (split.length > 2)
          throw new Error("Invalid integer: " + split.join("e"));
        if (split.length === 2) {
          var exp = split[1];
          if (exp[0] === "+")
            exp = exp.slice(1);
          exp = +exp;
          if (exp !== truncate(exp) || !isPrecise(exp))
            throw new Error("Invalid integer: " + exp + " is not a valid exponent.");
          var text = split[0];
          var decimalPlace = text.indexOf(".");
          if (decimalPlace >= 0) {
            exp -= text.length - decimalPlace - 1;
            text = text.slice(0, decimalPlace) + text.slice(decimalPlace + 1);
          }
          if (exp < 0)
            throw new Error("Cannot include negative exponent part for integers");
          text += new Array(exp + 1).join("0");
          v = text;
        }
        var isValid = /^([0-9][0-9]*)$/.test(v);
        if (!isValid)
          throw new Error("Invalid integer: " + v);
        if (supportsNativeBigInt) {
          return new NativeBigInt(BigInt(sign ? "-" + v : v));
        }
        var r = [], max2 = v.length, l = LOG_BASE, min2 = max2 - l;
        while (max2 > 0) {
          r.push(+v.slice(min2, max2));
          min2 -= l;
          if (min2 < 0)
            min2 = 0;
          max2 -= l;
        }
        trim(r);
        return new BigInteger(r, sign);
      }
      function parseNumberValue(v) {
        if (supportsNativeBigInt) {
          return new NativeBigInt(BigInt(v));
        }
        if (isPrecise(v)) {
          if (v !== truncate(v))
            throw new Error(v + " is not an integer.");
          return new SmallInteger(v);
        }
        return parseStringValue(v.toString());
      }
      function parseValue(v) {
        if (typeof v === "number") {
          return parseNumberValue(v);
        }
        if (typeof v === "string") {
          return parseStringValue(v);
        }
        if (typeof v === "bigint") {
          return new NativeBigInt(v);
        }
        return v;
      }
      for (var i = 0; i < 1e3; i++) {
        Integer[i] = parseValue(i);
        if (i > 0)
          Integer[-i] = parseValue(-i);
      }
      Integer.one = Integer[1];
      Integer.zero = Integer[0];
      Integer.minusOne = Integer[-1];
      Integer.max = max;
      Integer.min = min;
      Integer.gcd = gcd;
      Integer.lcm = lcm;
      Integer.isInstance = function(x) {
        return x instanceof BigInteger || x instanceof SmallInteger || x instanceof NativeBigInt;
      };
      Integer.randBetween = randBetween;
      Integer.fromArray = function(digits, base, isNegative) {
        return parseBaseFromArray(digits.map(parseValue), parseValue(base || 10), isNegative);
      };
      return Integer;
    }();
    if (typeof module !== "undefined" && module.hasOwnProperty("exports")) {
      module.exports = bigInt3;
    }
    if (typeof define === "function" && define.amd) {
      define(function() {
        return bigInt3;
      });
    }
  }
});

// node_modules/big-rational/BigRational.js
var require_BigRational = __commonJS({
  "node_modules/big-rational/BigRational.js"(exports, module) {
    var bigRat2 = function(bigInt3) {
      "use strict";
      function BigRational(num, denom) {
        if (denom.isZero())
          throw "Denominator cannot be 0.";
        this.numerator = this.num = num;
        this.denominator = this.denom = denom;
      }
      var gcd = bigInt3.gcd, lcm = bigInt3.lcm;
      function reduce(n, d) {
        var divisor = gcd(n, d), num = n.over(divisor), denom = d.over(divisor);
        if (denom.isNegative()) {
          return new BigRational(num.negate(), denom.negate());
        }
        return new BigRational(num, denom);
      }
      BigRational.prototype.add = function(n, d) {
        var v = interpret(n, d), multiple = lcm(this.denom, v.denom), a = multiple.divide(this.denom), b = multiple.divide(v.denom);
        a = this.num.times(a);
        b = v.num.times(b);
        return reduce(a.add(b), multiple);
      };
      BigRational.prototype.plus = BigRational.prototype.add;
      BigRational.prototype.subtract = function(n, d) {
        var v = interpret(n, d);
        return this.add(v.negate());
      };
      BigRational.prototype.minus = BigRational.prototype.subtract;
      BigRational.prototype.multiply = function(n, d) {
        var v = interpret(n, d);
        return reduce(this.num.times(v.num), this.denom.times(v.denom));
      };
      BigRational.prototype.times = BigRational.prototype.multiply;
      BigRational.prototype.divide = function(n, d) {
        var v = interpret(n, d);
        return reduce(this.num.times(v.denom), this.denom.times(v.num));
      };
      BigRational.prototype.over = BigRational.prototype.divide;
      BigRational.prototype.reciprocate = function() {
        return new BigRational(this.denom, this.num);
      };
      BigRational.prototype.mod = function(n, d) {
        var v = interpret(n, d);
        return this.minus(v.times(this.over(v).floor()));
      };
      BigRational.prototype.pow = function(n) {
        var v = bigInt3(n);
        var num = this.num.pow(v), denom = this.denom.pow(v);
        return reduce(num, denom);
      };
      BigRational.prototype.floor = function(toBigInt) {
        var divmod = this.num.divmod(this.denom), floor;
        if (divmod.remainder.isZero() || !divmod.quotient.isNegative()) {
          floor = divmod.quotient;
        } else
          floor = divmod.quotient.prev();
        if (toBigInt)
          return floor;
        return new BigRational(floor, bigInt3[1]);
      };
      BigRational.prototype.ceil = function(toBigInt) {
        var divmod = this.num.divmod(this.denom), ceil;
        if (divmod.remainder.isZero() || divmod.quotient.isNegative()) {
          ceil = divmod.quotient;
        } else
          ceil = divmod.quotient.next();
        if (toBigInt)
          return ceil;
        return new BigRational(ceil, bigInt3[1]);
      };
      BigRational.prototype.round = function(toBigInt) {
        return this.add(1, 2).floor(toBigInt);
      };
      BigRational.prototype.compareAbs = function(n, d) {
        var v = interpret(n, d);
        if (this.denom.equals(v.denom)) {
          return this.num.compareAbs(v.num);
        }
        return this.num.times(v.denom).compareAbs(v.num.times(this.denom));
      };
      BigRational.prototype.compare = function(n, d) {
        var v = interpret(n, d);
        if (this.denom.equals(v.denom)) {
          return this.num.compare(v.num);
        }
        var comparison = this.denom.isNegative() === v.denom.isNegative() ? 1 : -1;
        return comparison * this.num.times(v.denom).compare(v.num.times(this.denom));
      };
      BigRational.prototype.compareTo = BigRational.prototype.compare;
      BigRational.prototype.equals = function(n, d) {
        return this.compare(n, d) === 0;
      };
      BigRational.prototype.eq = BigRational.prototype.equals;
      BigRational.prototype.notEquals = function(n, d) {
        return this.compare(n, d) !== 0;
      };
      BigRational.prototype.neq = BigRational.prototype.notEquals;
      BigRational.prototype.lesser = function(n, d) {
        return this.compare(n, d) < 0;
      };
      BigRational.prototype.lt = BigRational.prototype.lesser;
      BigRational.prototype.lesserOrEquals = function(n, d) {
        return this.compare(n, d) <= 0;
      };
      BigRational.prototype.leq = BigRational.prototype.lesserOrEquals;
      BigRational.prototype.greater = function(n, d) {
        return this.compare(n, d) > 0;
      };
      BigRational.prototype.gt = BigRational.prototype.greater;
      BigRational.prototype.greaterOrEquals = function(n, d) {
        return this.compare(n, d) >= 0;
      };
      BigRational.prototype.geq = BigRational.prototype.greaterOrEquals;
      BigRational.prototype.abs = function() {
        if (this.isPositive())
          return this;
        return this.negate();
      };
      BigRational.prototype.negate = function() {
        if (this.denom.isNegative()) {
          return new BigRational(this.num, this.denom.negate());
        }
        return new BigRational(this.num.negate(), this.denom);
      };
      BigRational.prototype.isNegative = function() {
        return this.num.isNegative() !== this.denom.isNegative() && !this.num.isZero();
      };
      BigRational.prototype.isPositive = function() {
        return this.num.isNegative() === this.denom.isNegative() && !this.num.isZero();
      };
      BigRational.prototype.isZero = function() {
        return this.num.isZero();
      };
      BigRational.prototype.toDecimal = function(digits) {
        digits = typeof digits === "number" ? digits : 10;
        var n = this.num.divmod(this.denom);
        var intPart = n.quotient.abs().toString();
        var remainder = parse(n.remainder.abs(), this.denom);
        var shiftedRemainder = remainder.times(bigInt3("1e" + digits));
        var decPart = shiftedRemainder.num.over(shiftedRemainder.denom).toString();
        if (decPart.length < digits) {
          decPart = new Array(digits - decPart.length + 1).join("0") + decPart;
        }
        if (shiftedRemainder.num.mod(shiftedRemainder.denom).isZero()) {
          while (decPart.slice(-1) === "0") {
            decPart = decPart.slice(0, -1);
          }
        }
        if (digits < 1)
          decPart = "";
        if (this.isNegative()) {
          intPart = "-" + intPart;
        }
        if (decPart === "") {
          return intPart;
        }
        return intPart + "." + decPart;
      };
      BigRational.prototype.toString = function() {
        return String(this.num) + "/" + String(this.denom);
      };
      BigRational.prototype.valueOf = function() {
        if (!isFinite(+this.num) || !isFinite(+this.denom)) {
          return +this.toDecimal(64);
        }
        return this.num / this.denom;
      };
      function interpret(n, d) {
        return parse(n, d);
      }
      function parseDecimal(n) {
        var parts = n.split(/e/i);
        if (parts.length > 2) {
          throw new Error("Invalid input: too many 'e' tokens");
        }
        if (parts.length > 1) {
          var isPositive = true;
          if (parts[1][0] === "-") {
            parts[1] = parts[1].slice(1);
            isPositive = false;
          }
          if (parts[1][0] === "+") {
            parts[1] = parts[1].slice(1);
          }
          var significand = parseDecimal(parts[0]);
          var exponent = new BigRational(bigInt3(10).pow(parts[1]), bigInt3[1]);
          if (isPositive) {
            return significand.times(exponent);
          } else {
            return significand.over(exponent);
          }
        }
        parts = n.trim().split(".");
        if (parts.length > 2) {
          throw new Error("Invalid input: too many '.' tokens");
        }
        if (parts.length > 1) {
          var isNegative = parts[0][0] === "-";
          if (isNegative)
            parts[0] = parts[0].slice(1);
          var intPart = new BigRational(bigInt3(parts[0]), bigInt3[1]);
          var length = parts[1].length;
          while (parts[1][0] === "0") {
            parts[1] = parts[1].slice(1);
          }
          var exp = "1" + Array(length + 1).join("0");
          var decPart = reduce(bigInt3(parts[1]), bigInt3(exp));
          intPart = intPart.add(decPart);
          if (isNegative)
            intPart = intPart.negate();
          return intPart;
        }
        return new BigRational(bigInt3(n), bigInt3[1]);
      }
      function parse(a, b) {
        if (!a) {
          return new BigRational(bigInt3(0), bigInt3[1]);
        }
        if (b) {
          return reduce(bigInt3(a), bigInt3(b));
        }
        if (bigInt3.isInstance(a)) {
          return new BigRational(a, bigInt3[1]);
        }
        if (a instanceof BigRational)
          return a;
        var num;
        var denom;
        var text = String(a);
        var texts = text.split("/");
        if (texts.length > 2) {
          throw new Error("Invalid input: too many '/' tokens");
        }
        if (texts.length > 1) {
          var parts = texts[0].split("_");
          if (parts.length > 2) {
            throw new Error("Invalid input: too many '_' tokens");
          }
          if (parts.length > 1) {
            var isPositive = parts[0][0] !== "-";
            num = bigInt3(parts[0]).times(texts[1]);
            if (isPositive) {
              num = num.add(parts[1]);
            } else {
              num = num.subtract(parts[1]);
            }
            denom = bigInt3(texts[1]);
            return reduce(num, denom);
          }
          return reduce(bigInt3(texts[0]), bigInt3(texts[1]));
        }
        return parseDecimal(text);
      }
      parse.zero = parse(0);
      parse.one = parse(1);
      parse.minusOne = parse(-1);
      return parse;
    }(typeof bigInt !== "undefined" ? bigInt : require_BigInteger());
    if (typeof module !== "undefined") {
      if (module.hasOwnProperty("exports")) {
        module.exports = bigRat2;
      }
    }
  }
});

// src/source.js
var import_big_rational = __toESM(require_BigRational());
var import_big_integer = __toESM(require_BigInteger());
import_big_rational.default.min = (q1, q2) => q1.leq(q2) ? q1 : q2;
import_big_rational.default.max = (q1, q2) => q1.geq(q2) ? q1 : q2;
import_big_rational.default.clip = (q, qmin, qmax) => import_big_rational.default.max(qmin, import_big_rational.default.min(q, qmax));
var seconds_per_day = 86400;
var min_per_year = 525600;
var initial_period = 10;
var transition_period = 50;
var issuance_ratio_initial_min = (0, import_big_rational.default)(45, 1e3);
var issuance_ratio_initial_max = (0, import_big_rational.default)(55, 1e3);
var issuance_ratio_global_min = (0, import_big_rational.default)(25, 1e4);
var issuance_ratio_global_max = (0, import_big_rational.default)(10, 100);
var growth_rate = (0, import_big_rational.default)(1, 100);
var ratio_target = (0, import_big_rational.default)(50, 100);
var ratio_radius = (0, import_big_rational.default)(2, 100);
var Simulator = class {
  #storage_issuance_bonus = [];
  #storage_total_supply = [];
  #storage_total_supply_mask = [];
  #storage_total_delegated = [];
  #storage_total_delegated_mask = [];
  #storage_total_staked = [];
  #storage_total_staked_mask = [];
  #storage_cache_index = 0;
  set_total_supply(cycle, value) {
    this.#storage_cache_index = Math.min(cycle, this.#storage_cache_index);
    this.#storage_total_supply_mask[cycle] = value;
  }
  set_total_delegated(cycle, value) {
    this.#storage_cache_index = Math.min(cycle, this.#storage_cache_index);
    this.#storage_total_delegated_mask[cycle] = value;
  }
  set_total_staked(cycle, value) {
    this.#storage_cache_index = Math.min(cycle, this.#storage_cache_index);
    this.#storage_total_staked_mask[cycle] = value;
  }
  constructor(config) {
    this.config = config;
  }
  total_supply(cycle) {
    this.#prepare_for(cycle);
    return (0, import_big_integer.default)(this.#storage_total_supply[cycle]);
  }
  total_staked_balance(cycle) {
    if (cycle <= this.config.proto.consensus_rights_delay + 1) {
      return (0, import_big_integer.default)(
        this.#storage_total_staked_mask[cycle] ?? this.#storage_total_staked[this.config.proto.consensus_rights_delay + 1]
      );
    }
    this.#prepare_for(cycle - this.config.proto.consensus_rights_delay - 1);
    return (0, import_big_integer.default)(this.#storage_total_staked[cycle]);
  }
  total_delegated_balance(cycle) {
    this.#prepare_for(cycle);
    return (0, import_big_integer.default)(this.#storage_total_delegated[cycle]);
  }
  #compute_extremum(cycle, initial_value, final_value) {
    const trans = transition_period + 1;
    const t1 = this.config.chain.ai_activation_cycle + initial_period;
    const t2 = t1 + trans;
    if (cycle <= t1) {
      return initial_value;
    } else if (cycle >= t2) {
      return final_value;
    } else {
      const t = cycle - t1;
      const res = (0, import_big_rational.default)(t).multiply(final_value - initial_value).divide(trans).add(initial_value);
      return res;
    }
  }
  minimum_ratio(cycle) {
    return this.#compute_extremum(
      cycle,
      issuance_ratio_initial_min,
      issuance_ratio_global_min
    );
  }
  maximum_ratio(cycle) {
    return this.#compute_extremum(
      cycle,
      issuance_ratio_initial_max,
      issuance_ratio_global_max
    );
  }
  is_ai_activated(cycle) {
    return this.config.chain.ai_activation_cycle <= cycle;
  }
  get initial_period_start_cycle() {
    return this.config.chain.ai_activation_cycle;
  }
  get transition_period_start_cycle() {
    return this.config.chain.ai_activation_cycle + initial_period;
  }
  get final_period_start_cycle() {
    return this.config.chain.ai_activation_cycle + initial_period + transition_period;
  }
  is_in_initial_period(cycle) {
    const l = this.config.chain.ai_activation_cycle <= cycle;
    const r = cycle <= initial_period + this.config.chain.ai_activation_cycle;
    return l && r;
  }
  is_in_transition_period(cycle) {
    const l = initial_period + this.config.chain.ai_activation_cycle < cycle;
    const r = cycle <= initial_period + transition_period + this.config.chain.ai_activation_cycle;
    return l && r;
  }
  is_in_final_period(cycle) {
    return cycle > initial_period + transition_period + this.config.chain.ai_activation_cycle;
  }
  set_staked_ratio_at(cycle, value) {
    this.#prepare_for(cycle);
    this.#storage_issuance_bonus = [];
    this.#storage_cache_index = Math.min(cycle, this.#storage_cache_index);
    const ratio = import_big_rational.default.clip((0, import_big_rational.default)(value), import_big_rational.default.zero, import_big_rational.default.one);
    const i = cycle + this.config.proto.consensus_rights_delay + 1;
    const total_supply = this.#storage_total_supply[i];
    const total_staked = this.#storage_total_staked[i];
    const total_delegated = this.#storage_total_delegated[i];
    const new_value = ratio.multiply(total_supply).ceil();
    const diff_staked = new_value - total_staked;
    const diff_delegated = this.#storage_total_delegated[i] - diff_staked;
    const new_delegated = Math.max(diff_delegated, 0);
    const new_staked = diff_delegated < 0 ? new_value + diff_delegated : new_value;
    this.#storage_total_staked_mask[i] = new_staked;
    this.#storage_total_delegated_mask[i] = new_delegated;
  }
  staked_ratio_for_next_cycle(cycle) {
    const total_supply = this.total_supply(cycle);
    const total_frozen_stake = this.total_staked_balance(
      cycle + this.config.proto.consensus_rights_delay + 1
    );
    return (0, import_big_rational.default)(total_frozen_stake).divide(total_supply);
  }
  static_rate_for_next_cycle(cycle) {
    const next_cycle = cycle + 1;
    const staked_ratio = this.staked_ratio_for_next_cycle(cycle);
    const ratio_min = this.minimum_ratio(next_cycle);
    const ratio_max = this.maximum_ratio(next_cycle);
    const static_rate = staked_ratio.eq(0) ? ratio_max : (0, import_big_rational.default)(1, 1600).multiply(import_big_rational.default.one.divide(staked_ratio.pow(2)));
    return import_big_rational.default.clip(static_rate, ratio_min, ratio_max);
  }
  dynamic_rate_for_next_cycle(cycle) {
    if (cycle < this.config.chain.ai_activation_cycle) {
      return import_big_rational.default.zero;
    }
    if (this.#storage_issuance_bonus[cycle]) {
      return (0, import_big_rational.default)(this.#storage_issuance_bonus[cycle]);
    }
    const previous_bonus = this.dynamic_rate_for_next_cycle(cycle - 1);
    const staked_ratio = this.staked_ratio_for_next_cycle(cycle);
    const new_cycle = cycle + 1;
    const ratio_max = this.maximum_ratio(new_cycle);
    const static_rate = this.static_rate_for_next_cycle(cycle);
    const static_rate_dist_to_max = ratio_max.minus(static_rate);
    const udist = import_big_rational.default.max(
      import_big_rational.default.zero,
      staked_ratio.minus(ratio_target).abs().minus(ratio_radius)
    );
    const dist = staked_ratio.geq(ratio_target) ? udist.negate() : udist;
    const seconds_per_cycle = this.config.proto.blocks_per_cycle * this.config.proto.minimal_block_delay;
    const days_per_cycle = (0, import_big_rational.default)(seconds_per_cycle).divide(seconds_per_day);
    const max_new_bonus = import_big_rational.default.min(
      static_rate_dist_to_max,
      this.config.proto.max_bonus
    );
    let new_bonus = previous_bonus.add(
      dist.multiply(growth_rate).multiply(days_per_cycle)
    );
    new_bonus = import_big_rational.default.clip(new_bonus, import_big_rational.default.zero, max_new_bonus);
    console.assert(0 <= new_bonus && new_bonus <= this.config.proto.max_bonus);
    this.#storage_issuance_bonus[cycle] = new_bonus;
    return (0, import_big_rational.default)(new_bonus);
  }
  issuance_rate_for_next_cycle(cycle) {
    const next_cycle = cycle + 1;
    const ratio_min = this.minimum_ratio(next_cycle);
    const ratio_max = this.maximum_ratio(next_cycle);
    const static_rate = this.static_rate_for_next_cycle(cycle);
    const bonus = this.dynamic_rate_for_next_cycle(cycle);
    return import_big_rational.default.clip(static_rate.add(bonus), ratio_min, ratio_max);
  }
  reward_coeff(cycle) {
    if (cycle <= this.config.chain.ai_activation_cycle + this.config.proto.consensus_rights_delay) {
      return import_big_rational.default.one;
    }
    const adjusted_cycle = cycle - this.config.proto.consensus_rights_delay - 1;
    const issuance_rate = this.issuance_rate_for_next_cycle(adjusted_cycle);
    const total_supply = this.total_supply(adjusted_cycle);
    return issuance_rate.multiply(
      (0, import_big_rational.default)(total_supply).divide(
        (0, import_big_rational.default)(this.config.proto.base_total_issued_per_minute).multiply(
          min_per_year
        )
      )
    );
  }
  #sum_rewards_weight() {
    return this.config.proto.attestation_rewards + this.config.proto.fixed_baking_rewards + this.config.proto.bonus_baking_rewards + this.config.proto.nonce_revelation_tip + this.config.proto.vdf_tip;
  }
  #tez_from_weights(weight) {
    const num = (0, import_big_integer.default)(weight).multiply(this.config.proto.minimal_block_delay);
    const den = (0, import_big_integer.default)(this.#sum_rewards_weight()).multiply(60);
    const res = (0, import_big_integer.default)(this.config.proto.base_total_issued_per_minute).multiply(num).divide(den);
    return res;
  }
  #reward_from_constants(cycle, weight, d = 1) {
    const coeff = (0, import_big_rational.default)(this.reward_coeff(cycle));
    const rewards = this.#tez_from_weights(weight);
    const base_rewards = rewards.divide(d);
    return base_rewards.multiply(coeff.numerator).divide(coeff.denominator);
  }
  baking_reward_fixed_portion(cycle) {
    return this.#reward_from_constants(
      cycle,
      this.config.proto.fixed_baking_rewards
    );
  }
  baking_reward_bonus_per_slot(cycle) {
    const bonus_committee_size = this.config.proto.consensus_committee_size - this.config.proto.consensus_threshold;
    if (bonus_committee_size == 0) {
      return 0;
    }
    return this.#reward_from_constants(
      cycle,
      this.config.proto.bonus_baking_rewards,
      bonus_committee_size
    );
  }
  attestation_reward_per_slot(cycle) {
    return this.#reward_from_constants(
      cycle,
      this.config.proto.attestation_rewards,
      this.config.proto.consensus_committee_size
    );
  }
  seed_nonce_revelation_tip(cycle) {
    return this.#reward_from_constants(
      cycle,
      this.config.proto.nonce_revelation_tip * this.config.proto.blocks_per_commitment
    );
  }
  vdf_revelation_tip(cycle) {
    return this.#reward_from_constants(
      cycle,
      this.config.proto.vdf_tip * this.config.proto.blocks_per_commitment
    );
  }
  #current_rewards_per_minute(cycle) {
    return this.reward_coeff(cycle).times(
      this.config.proto.base_total_issued_per_minute
    );
  }
  current_yearly_rate_value(cycle) {
    return this.#current_rewards_per_minute(cycle).divide(this.total_supply(cycle)).times(min_per_year).times(100);
  }
  issuance_per_block(cycle) {
    const baking_reward_fixed_portion = this.baking_reward_fixed_portion(cycle);
    const baking_reward_bonus_per_block = this.#reward_from_constants(
      cycle,
      this.config.proto.bonus_baking_rewards
    );
    const attestation_rewards_per_block = this.#reward_from_constants(
      cycle,
      this.config.proto.attestation_rewards
    );
    const vdf_revelation_tip = this.#reward_from_constants(
      cycle,
      this.config.proto.vdf_tip
    );
    const seed_nonce_revelation_tip = this.#reward_from_constants(
      cycle,
      this.config.proto.nonce_revelation_tip
    );
    return baking_reward_fixed_portion + baking_reward_bonus_per_block + attestation_rewards_per_block + vdf_revelation_tip + seed_nonce_revelation_tip;
  }
  issuance_per_cycle(cycle) {
    return this.issuance_per_block(cycle) * this.config.proto.blocks_per_cycle;
  }
  #compute_new_balances(cycle) {
    if (cycle < 0) {
      return;
    }
    if (cycle == 0) {
      this.#storage_total_supply[0] = this.#storage_total_supply_mask[0];
      this.#storage_total_delegated[0] = this.#storage_total_delegated_mask[0];
      this.#storage_total_staked[this.config.proto.consensus_rights_delay + 1] = this.#storage_total_staked_mask[this.config.proto.consensus_rights_delay + 1];
      return;
    }
    const issuance = this.issuance_per_cycle(cycle - 1);
    const new_supply = this.#storage_total_supply_mask[cycle] ?? this.total_supply(cycle - 1).add(issuance);
    this.#storage_total_supply[cycle] = new_supply;
    const delegated = this.total_delegated_balance(cycle - 1);
    const staked = this.total_staked_balance(
      cycle + this.config.proto.consensus_rights_delay
    );
    if (this.is_ai_activated(cycle)) {
      const ratio_for_delegated = (0, import_big_rational.default)(delegated).divide(
        staked.times(2).add(delegated)
      );
      const ratio_for_staked = (0, import_big_rational.default)(staked).times(2).divide(staked.times(2).add(delegated));
      const new_delegated = this.#storage_total_delegated_mask[cycle] ?? ratio_for_delegated.multiply(issuance).add(delegated).round().valueOf();
      this.#storage_total_delegated[cycle] = new_delegated;
      const new_staked = this.#storage_total_staked_mask[cycle + this.config.proto.consensus_rights_delay + 1] ?? ratio_for_staked.multiply(issuance).add(staked).round().valueOf();
      this.#storage_total_staked[cycle + this.config.proto.consensus_rights_delay + 1] = new_staked;
    } else {
      const new_delegated = this.#storage_total_delegated_mask[cycle] ?? delegated.add(issuance);
      this.#storage_total_delegated[cycle] = new_delegated;
      const new_staked = this.#storage_total_staked_mask[cycle + this.config.proto.consensus_rights_delay + 1] ?? this.#storage_total_staked[cycle + this.config.proto.consensus_rights_delay];
      this.#storage_total_staked[cycle + this.config.proto.consensus_rights_delay + 1] = new_staked;
    }
  }
  #prepare_for(cycle) {
    for (let c = this.#storage_cache_index; c <= cycle; c++) {
      this.#compute_new_balances(c);
      this.#storage_cache_index++;
    }
  }
};
var Delegate = class {
  #storage_cache_index = 0;
  #registration_cycle = null;
  #storage_own_staked_balance = [0];
  #storage_third_party_staked_balance = [0];
  #storage_own_spendable_balance = [0];
  #storage_third_party_delegated_balance = [0];
  #storage_own_staked_balance_mask = [];
  #storage_third_party_staked_balance_mask = [];
  #storage_own_spendable_balance_mask = [];
  #storage_third_party_delegated_balance_mask = [];
  constructor(simulator, config) {
    this.simulator = simulator;
    this.config = config;
  }
  clear() {
    this.#storage_cache_index = 0;
    this.#storage_own_staked_balance = [0];
    this.#storage_third_party_staked_balance = [0];
    this.#storage_own_spendable_balance = [0];
    this.#storage_third_party_delegated_balance = [0];
  }
  #own_staked_balance(cycle) {
    return this.#storage_own_staked_balance_mask[cycle] ?? this.#storage_own_staked_balance[cycle];
  }
  #third_party_staked_balance(cycle) {
    return this.#storage_third_party_staked_balance_mask[cycle] ?? this.#storage_third_party_staked_balance[cycle];
  }
  #own_spendable_balance(cycle) {
    return this.#storage_own_spendable_balance_mask[cycle] ?? this.#storage_own_spendable_balance[cycle];
  }
  #third_party_delegated_balance(cycle) {
    return this.#storage_third_party_delegated_balance_mask[cycle] ?? this.#storage_third_party_delegated_balance[cycle];
  }
  // The storage_cache_index is used to register what are the values
  // still valid or the ones that need to be recomputed. Hence, for any change
  // that occurs at a certain level we need to recompute each data above this level.
  // This is why we we take the minimal value here
  set_own_spendable_balance(cycle, value) {
    this.#storage_own_spendable_balance_mask[cycle] = value;
    this.#storage_cache_index = Math.min(this.#storage_cache_index, cycle);
  }
  set_third_party_delegated_balance(cycle, value) {
    this.#storage_third_party_delegated_balance_mask[cycle] = value;
    this.#storage_cache_index = Math.min(this.#storage_cache_index, cycle);
  }
  set_own_staked_balance(cycle, value) {
    this.#storage_own_staked_balance_mask[cycle] = value;
    this.#storage_cache_index = Math.min(this.#storage_cache_index, cycle);
  }
  set_third_party_staked_balance(cycle, value) {
    if (!this.simulator.is_ai_activated(cycle)) {
      return;
    }
    this.#storage_third_party_staked_balance_mask[cycle] = value;
    this.#storage_cache_index = Math.min(this.#storage_cache_index, cycle);
  }
  is_activated(cycle) {
    const activation_cycle = this.config.delegate_registration_cycle + this.simulator.config.proto.consensus_rights_delay + 1;
    return activation_cycle <= cycle;
  }
  delegate_info(cycle) {
    if (cycle < 0) {
      return {
        own_staked: import_big_integer.default.zero,
        considered_staked: import_big_integer.default.zero,
        over_staked: import_big_integer.default.zero,
        available_staked: import_big_integer.default.zero,
        considered_delegated: import_big_integer.default.zero,
        over_delegated: import_big_integer.default.zero,
        available_delegated: import_big_integer.default.zero
      };
    }
    this.#prepare_for(cycle);
    const own_staked = (0, import_big_integer.default)(this.#own_staked_balance(cycle));
    const third_party_staked = (0, import_big_integer.default)(this.#third_party_staked_balance(cycle));
    const considered_limit_of_staking = Math.min(
      this.config.delegate_policy.limit_of_staking_over_baking,
      this.simulator.config.proto.max_limit_of_staking_over_baking
    );
    const max_third_party_staked = own_staked.times(
      considered_limit_of_staking
    );
    const considered_staked = import_big_integer.default.min(
      own_staked.add(third_party_staked),
      own_staked.add(max_third_party_staked)
    );
    const stake_diff = max_third_party_staked.minus(third_party_staked);
    const over_staked = stake_diff.isPositive() ? import_big_integer.default.zero : stake_diff.negate();
    const available_staked = this.simulator.is_ai_activated(cycle) && stake_diff.isPositive() ? stake_diff : import_big_integer.default.zero;
    const own_delegated = (0, import_big_integer.default)(this.#own_spendable_balance(cycle));
    const third_party_delegated = (0, import_big_integer.default)(
      this.#third_party_delegated_balance(cycle)
    );
    const extended_delegated = own_delegated.add(third_party_delegated).add(over_staked);
    const max_delegated = own_staked.times(9);
    const delegated_diff = max_delegated.minus(extended_delegated);
    const over_delegated = delegated_diff.isPositive() ? import_big_integer.default.zero : delegated_diff.negate();
    const available_delegated = delegated_diff.isPositive() ? delegated_diff : import_big_integer.default.zero;
    const considered_delegated = import_big_integer.default.min(max_delegated, extended_delegated);
    return {
      own_staked,
      considered_staked,
      over_staked,
      available_staked,
      considered_delegated,
      over_delegated,
      available_delegated
    };
  }
  baking_power(cycle) {
    const adjusted_cycle = cycle - this.simulator.config.proto.consensus_rights_delay - 1;
    if (adjusted_cycle < 0 || !this.is_activated(cycle)) {
      return {
        baking_power: import_big_rational.default.zero,
        ratio_for_staking: import_big_rational.default.zero,
        ratio_for_delegating: import_big_rational.default.zero
      };
    }
    const { considered_staked, considered_delegated } = this.delegate_info(adjusted_cycle);
    const total_staked = this.simulator.total_staked_balance(adjusted_cycle);
    const total_delegated = this.simulator.total_delegated_balance(adjusted_cycle);
    const num = this.simulator.is_ai_activated(adjusted_cycle) ? considered_staked.times(2).add(considered_delegated) : considered_staked.add(considered_delegated);
    const den = this.simulator.is_ai_activated(adjusted_cycle) ? total_staked.times(2).add(total_delegated) : total_staked.add(total_delegated);
    const baking_power = (0, import_big_rational.default)(num).divide(den);
    const ratio_denum = considered_staked.times(2).add(considered_delegated);
    const ratio_for_staking = ratio_denum.isZero() || !this.simulator.is_ai_activated(adjusted_cycle) ? import_big_rational.default.zero : (0, import_big_rational.default)(considered_staked.times(2)).divide(ratio_denum);
    const ratio_for_delegating = ratio_denum.isZero() ? import_big_rational.default.zero : !this.simulator.is_ai_activated(adjusted_cycle) ? import_big_rational.default.one : (0, import_big_rational.default)(considered_delegated).divide(ratio_denum);
    return { baking_power, ratio_for_staking, ratio_for_delegating };
  }
  estimated_rewards(cycle) {
    const { baking_power, ratio_for_staking, ratio_for_delegating } = this.baking_power(cycle);
    const estimated_number_of_blocks_baked = baking_power.times(this.simulator.config.proto.blocks_per_cycle).ceil().valueOf();
    const estimated_rewards_for_fixed_portion_baking = estimated_number_of_blocks_baked * this.simulator.baking_reward_fixed_portion(cycle);
    const bonus_committee_size = this.simulator.config.proto.consensus_committee_size - this.simulator.config.proto.consensus_threshold;
    const estimated_rewards_for_baking_bonus = estimated_number_of_blocks_baked * this.simulator.baking_reward_bonus_per_slot(cycle).times(bonus_committee_size);
    const estimated_number_of_attestations = baking_power.times(this.simulator.config.proto.consensus_committee_size).times(this.simulator.config.proto.blocks_per_cycle).ceil().valueOf();
    const estimated_rewards_for_attestations = estimated_number_of_attestations * this.simulator.attestation_reward_per_slot(cycle);
    const estimated_rewards_for_nonce_revelation = estimated_number_of_blocks_baked / this.simulator.config.proto.blocks_per_commitment * this.simulator.seed_nonce_revelation_tip(cycle);
    const estimated_rewards_for_vdf_revelation = estimated_number_of_blocks_baked / this.simulator.config.proto.blocks_per_commitment * this.simulator.vdf_revelation_tip(cycle);
    const estimated_total_rewards = estimated_rewards_for_fixed_portion_baking + estimated_rewards_for_baking_bonus + estimated_rewards_for_attestations + estimated_rewards_for_nonce_revelation + estimated_rewards_for_vdf_revelation;
    const estimated_rewards_from_delegating = Math.ceil(
      ratio_for_delegating * estimated_total_rewards
    );
    const estimated_rewards_from_staking = ratio_for_staking.times(
      estimated_total_rewards
    );
    const { own_staked, considered_staked } = this.delegate_info(cycle);
    const ratio_own_stake = considered_staked.isZero() ? import_big_rational.default.zero : (0, import_big_rational.default)(own_staked, considered_staked);
    const ratio_third_party_stake = considered_staked.isZero() ? import_big_rational.default.zero : import_big_rational.default.one.minus(ratio_own_stake);
    const estimated_rewards_from_own_staking = ratio_own_stake.times(estimated_rewards_from_staking).ceil().valueOf();
    const estimated_rewards_from_third_party_staking_raw = ratio_third_party_stake.times(estimated_rewards_from_staking);
    const estimated_rewards_from_edge_of_baking_over_staking = estimated_rewards_from_third_party_staking_raw.times(this.config.delegate_policy.edge_of_baking_over_staking).ceil().valueOf();
    const estimated_rewards_from_third_party_staking = estimated_rewards_from_third_party_staking_raw.minus(estimated_rewards_from_edge_of_baking_over_staking).ceil().valueOf();
    return {
      estimated_number_of_blocks_baked,
      estimated_number_of_attestations,
      estimated_rewards_for_fixed_portion_baking,
      estimated_rewards_for_baking_bonus,
      estimated_rewards_for_attestations,
      estimated_rewards_for_nonce_revelation,
      estimated_rewards_for_vdf_revelation,
      estimated_rewards_from_own_staking,
      estimated_rewards_from_third_party_staking,
      estimated_rewards_from_delegating,
      estimated_rewards_from_edge_of_baking_over_staking,
      estimated_total_rewards
    };
  }
  #compute_new_balances(cycle) {
    if (cycle < 0) {
      return;
    }
    if (cycle == 0) {
      this.#storage_own_staked_balance[0] = this.#storage_own_staked_balance_mask[0] ?? 0;
      this.#storage_third_party_staked_balance[0] = this.#storage_third_party_staked_balance_mask[0] ?? 0;
      this.#storage_own_spendable_balance[0] = this.#storage_own_spendable_balance_mask[0] ?? 0;
      this.#storage_third_party_delegated_balance[0] = this.#storage_third_party_delegated_balance_mask[0] ?? 0;
      return;
    }
    const rewards = this.estimated_rewards(cycle - 1);
    if (!this.simulator.is_ai_activated(cycle)) {
      const previous_own_staked_balance = this.#storage_own_staked_balance[cycle - 1];
      const previous_third_party_delegated = this.#storage_third_party_delegated_balance[cycle - 1];
      const previous_own_spendable_balance = this.#storage_own_spendable_balance[cycle - 1];
      const previous_total_delegated = previous_own_spendable_balance + previous_third_party_delegated;
      const new_own_staked_balance = Math.round(
        1 / 10 * (previous_own_staked_balance + previous_total_delegated + rewards.estimated_total_rewards)
      );
      const new_own_spendable_balance = previous_own_spendable_balance - (previous_own_staked_balance - new_own_staked_balance);
      this.#storage_own_staked_balance[cycle] = this.#storage_own_staked_balance_mask[cycle] ?? new_own_staked_balance;
      this.#storage_own_spendable_balance[cycle] = this.#storage_own_spendable_balance_mask[cycle] ?? new_own_spendable_balance;
      this.#storage_third_party_delegated_balance[cycle] = this.#storage_third_party_delegated_balance_mask[cycle] ?? this.#storage_third_party_delegated_balance[cycle - 1];
      this.#storage_third_party_staked_balance[cycle] = this.#storage_third_party_staked_balance_mask[cycle] ?? this.#storage_third_party_staked_balance[cycle - 1];
    } else {
      this.#storage_own_staked_balance[cycle] = this.#storage_own_staked_balance_mask[cycle] ?? this.#storage_own_staked_balance[cycle - 1] + rewards.estimated_rewards_from_own_staking + rewards.estimated_rewards_from_edge_of_baking_over_staking;
      this.#storage_third_party_staked_balance[cycle] = this.#storage_third_party_staked_balance_mask[cycle] ?? this.#storage_third_party_staked_balance[cycle - 1] + rewards.estimated_rewards_from_third_party_staking;
      this.#storage_own_spendable_balance[cycle] = this.#storage_own_spendable_balance_mask[cycle] ?? this.#storage_own_spendable_balance[cycle - 1] + rewards.estimated_rewards_from_delegating;
      this.#storage_third_party_delegated_balance[cycle] = this.#storage_third_party_delegated_balance_mask[cycle] ?? this.#storage_third_party_delegated_balance[cycle - 1];
    }
  }
  #prepare_for(cycle) {
    for (let i = this.#storage_cache_index; i <= cycle; i++) {
      this.#compute_new_balances(i);
      this.#storage_cache_index++;
    }
  }
  estimated_own_staked_balance(cycle) {
    this.#prepare_for(cycle);
    return this.#own_staked_balance(cycle);
  }
  estimated_third_party_staked_balance(cycle) {
    this.#prepare_for(cycle);
    return this.#third_party_staked_balance(cycle);
  }
  estimated_own_spendable_balance(cycle) {
    this.#prepare_for(cycle);
    return this.#own_spendable_balance(cycle);
  }
  third_party_delegated_balance(cycle) {
    this.#prepare_for(cycle);
    return this.#third_party_delegated_balance(cycle);
  }
  ratio_own_staked_spendable_balance(cycle) {
    this.#prepare_for(cycle);
    return 100 * (this.#own_staked_balance(cycle) / (this.#own_staked_balance(cycle) + this.#own_spendable_balance(cycle)));
  }
  ratio_third_party_staked_delegated_balance(cycle) {
    this.#prepare_for(cycle);
    return 100 * (this.#third_party_staked_balance(cycle) / (this.#third_party_delegated_balance(cycle) + this.#third_party_staked_balance(cycle)));
  }
  set_ratio_own_staked_spendable_balance(cycle, value) {
    if (!this.simulator.is_ai_activated(cycle)) {
      return;
    }
    const staked = this.#own_staked_balance(cycle);
    const spendable = this.#own_spendable_balance(cycle);
    const den = staked + spendable;
    const new_staked = Math.round(value / 100 * den);
    const new_spendable = den - new_staked;
    this.set_own_staked_balance(cycle, new_staked);
    this.set_own_spendable_balance(cycle, new_spendable);
  }
  set_ratio_third_party_staked_delegated_balance(cycle, value) {
    if (!this.simulator.is_ai_activated(cycle)) {
      return;
    }
    if (this.config.delegate_policy.limit_of_staking_over_baking == 0) {
      return;
    }
    const staked = this.#third_party_staked_balance(cycle);
    const delegated = this.#third_party_delegated_balance(cycle);
    const den = staked + delegated;
    const new_staked = Math.round(value / 100 * den);
    const new_delegated = den - new_staked;
    this.set_third_party_staked_balance(cycle, new_staked);
    this.set_third_party_delegated_balance(cycle, new_delegated);
  }
};
export {
  Delegate,
  Simulator
};
